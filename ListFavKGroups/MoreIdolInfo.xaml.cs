﻿using System;
using System.Collections.Generic;
using ListFavKGroups.Models;

using Xamarin.Forms;

namespace ListFavKGroups
{
    public partial class MoreIdolInfo : ContentPage
    {
        public MoreIdolInfo()
        {
            InitializeComponent();
        }
        public MoreIdolInfo(ImageCellItem idol)
        {
            InitializeComponent();
            BindingContext = idol;
        }
    }
}