﻿using Xamarin.Forms;

namespace ListFavKGroups
{
    public partial class ListFavKGroupsPage : ContentPage
    {
        public ListFavKGroupsPage()
        {
            InitializeComponent();
        }
        void Handle_BTS(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new LVBTS());
        }
        void Handle_BAP(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new LVBAP());
        }

    }
}
