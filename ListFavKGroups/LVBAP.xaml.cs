using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ListFavKGroups.Models;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace ListFavKGroups
{
    public partial class LVBAP : ContentPage
    {
        
        public LVBAP()
        {
            
            InitializeComponent();
            PopulateList();
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            BAPListView.IsRefreshing = false;
            PopulateList();
        }

        public void MoreClicked(object sender,System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var idol = (ImageCellItem)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreIdolInfo(idol));
        }
        void DeleteClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var idol = (ObservableCollection<ImageCellItem>)BAPListView.ItemsSource;
            int position = (int)menuItem.CommandParameter;
            int place = FindPos(position, idol);
            idol.RemoveAt(place);

        }
        protected int FindPos(int pos, ObservableCollection<ImageCellItem> item)
        {
            int x = 0;
            foreach(ImageCellItem area in item)
            {
                if(pos == area.position)
                {
                    return x;
                }
                x++;
            }
            return -1;
        }
        void Handle_Group(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            
            var listView = (ListView)sender;
            ImageCellItem itemTapped = (ImageCellItem)listView.SelectedItem;
            var uri = new Uri(itemTapped.url);
            Device.OpenUri(uri);
        }
        private void PopulateList()
        {
            var imagesForLVBAP = new ObservableCollection<ImageCellItem>()
            {
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("himchan.jpg"),
                    ImageText = "Kim Himchan",
                    SubtitleText = "stage name: Himchan, Position: Vocalist",
                    url = "http://itsbap.com/himchan"
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("yongguk.jpg"),
                    ImageText = "Bang Yongguk",
                    SubtitleText = "Stage name= Yongguk, Position: Rapper/Leader",
                    url = "http://itsbap.com/yongguk"

                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("daehyun.jpg"),
                    ImageText = "Jung Daehyun",
                    SubtitleText = "Stage name= Daehyun, Position: Vocalist",
                    url = "http://itsbap.com/daehyun"
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("youngjae.jpg"),
                    ImageText = "Yoo Youngjae",
                    SubtitleText = "Stage name: Youngjae, Position: Vocalist",
                    url = "http://itsbap.com/youngjae"

                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("jongup.jpg"),
                    ImageText = "Moon Jongup",
                    SubtitleText = "Stage name: Jongup, Position: Vocalist/Dancer",
                    url = "http://itsbap.com/jongup"
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("zelo.jpg"),
                    ImageText = "Choi Junhong",
                    SubtitleText = "Stage Name= Zelo, Position: Rapper/Dancer",
                    url = "http://itsbap.com/zelo"
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("handups.jpg"),
                    ImageText = "Hands Up",
                    SubtitleText = "Trust me it's a bop",
                    url = "https://www.youtube.com/watch?v=Cd8UxUlqaZ8"
                },
            };
            BAPListView.ItemsSource = imagesForLVBAP;
        }
    }
}