using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ListFavKGroups.Models;
using Xamarin.Forms;

namespace ListFavKGroups
{
    public partial class LVBTS : ContentPage
    {
        public LVBTS()
        {
            InitializeComponent();

            PopulateColorList();

        }
        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            BTSListView.IsRefreshing = false;
            PopulateColorList();
        }
        public void MoreClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var idol = (ImageCellItem)menuItem.CommandParameter;
            Navigation.PushAsync(new MoreIdolInfo(idol));
        }
        void DeleteClicked(object sender, System.EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var idol = (ObservableCollection<ImageCellItem>)BTSListView.ItemsSource;
            int position = (int)menuItem.CommandParameter;
            int place = FindPos(position, idol);
            idol.RemoveAt(place);

        }
        protected int FindPos(int pos, ObservableCollection<ImageCellItem> item)
        {
            int x = 0;
            foreach (ImageCellItem area in item)
            {
                if (pos == area.position)
                {
                    return x;
                }
                x++;
            }
            return -1;
        }
        void Handle_Member(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var listView = (ListView)sender;
            ImageCellItem itemTapped = (ImageCellItem)listView.SelectedItem;
            var uri = new Uri(itemTapped.url);
            Device.OpenUri(uri);
        }
        private void PopulateColorList()
        {
            var imagesForLVBTS = new ObservableCollection<ImageCellItem>()
            {
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("seokjin (1).jpg"),
                    ImageText = "Kim Seokjin",
                    SubtitleText="stage name: Jin, Position: Vocalist",
                    url = "http://bangtanboys.wikia.com/wiki/Jin"
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("yoongi.jpeg"),
                    ImageText = "Min Yoongi",
                    SubtitleText = "Stage name= Suga, Position: Rapper",
                    url = "http://bangtanboys.wikia.com/wiki/Suga"

                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("hoseok.jpg"),
                    ImageText = "Jung Hoseok",
                    SubtitleText = "Stage name= J-Hope, Position: Dancer/Rapper",
                    url = "http://bangtanboys.wikia.com/wiki/J-Hope"
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("rm.jpg"),
                    ImageText = "Kim Namjoon",
                    SubtitleText = "Stage name: RM, Position: Leader/Rapper",
                    url = "http://bangtanboys.wikia.com/wiki/RM"

                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("jimin.jpg"),
                    ImageText = "Park Jimin",
                    SubtitleText = "Stage name: Jimin, Position: Vocalist/Dancer",
                    url = "http://bangtanboys.wikia.com/wiki/Jimin"
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("V.jpg"),
                    ImageText = "Kim Taehyung",
                    SubtitleText = "Stage Name= V, Position: Vocalist/Actor",
                    url = "http://bangtanboys.wikia.com/wiki/V"
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("jungkookie.jpg"),
                    ImageText = "Jeon Jungkook",
                    SubtitleText = "Stage name= Jungkook, Position: Vocalist/Rapper/Dancer",
                    url = "http://bangtanboys.wikia.com/wiki/Jungkook"
                },
                new ImageCellItem()
                {
                    IconSource = ImageSource.FromFile("micdrop.jpg"),
                    ImageText = "Mic Drop ft. Steve Aoki",
                    SubtitleText = "Trust me it's a bop",
                    url = "https://www.youtube.com/watch?v=kTlv5_Bs8aw"
                }
            };
            BTSListView.ItemsSource = imagesForLVBTS;
        }
    }
}