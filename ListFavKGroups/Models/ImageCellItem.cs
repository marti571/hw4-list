using System;

using Xamarin.Forms;

namespace ListFavKGroups.Models
{
    public class ImageCellItem
    {
        public string ImageText
        {
            get;
            set;
        }
        public ImageSource IconSource
        {
            get;
            set;
        }
        public string SubtitleText
        {
            get;
            set;
        }

        public string url
        {
            get;
            set;
        }
        public int position
        {
            get;
            set;
        }
    }
}

